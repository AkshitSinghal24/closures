
function counterFactory() {
    let count=0;
    function increase() {
      count += 1;
      return count;
    }
    return {
      increment: increase,
      decrement: function(){
        count -= 1;
        return count;
      },
    };
  }

module.exports = counterFactory;
