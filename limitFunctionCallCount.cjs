function limitFunctionCallCount(cb, n) {
  if (cb === undefined || n === undefined) {
    throw new Error("Pass Parameters Correctly");
  }
  return function (...args) {
    if (n > 0) {
      n--;
      return cb(...args);
    } else return null;
  };

  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
}

module.exports = limitFunctionCallCount;
